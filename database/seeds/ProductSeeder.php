<?php

use App\SharedModels\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Product::all()->count() === 0) {
            factory(Product::class)->times(10)->create();
        }
    }
}
