<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Order;
use App\SharedModels\Product;

$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'product_id' => $faker->unique()->numberBetween(1, 10),
        'name' => $faker->word,
        'price' => $faker->randomFloat(2, 5, 1000)
    ];
});

$factory->define(Order::class, function (\Faker\Generator $faker) {
    return [
        'total_sum' => $faker->randomFloat(2, 5, 1000),
        'paid' => $faker->boolean,
        'billed' => $faker->boolean,
        'user_id' => $faker->uuid,
        'product_id' => $faker->numberBetween(1, 10)
    ];
});
