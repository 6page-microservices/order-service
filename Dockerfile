FROM composer:latest as build
WORKDIR /app
COPY . /app
RUN docker-php-ext-install pdo_mysql bcmath sockets
RUN composer install

FROM php:7.3-apache
EXPOSE 80
# TODO: Find better solution instead of duplicating installs between composer and php image
RUN docker-php-ext-install pdo_mysql bcmath sockets
COPY --from=build /app /var/www/html
RUN chown -R www-data:www-data /var/www/html && a2enmod rewrite
# See: https://hub.docker.com/_/php?tab=description
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
# Command extended based on https://github.com/docker-library/php/blob/master/7.3/stretch/apache/Dockerfile
CMD ["sh","-c","php artisan migrate --seed --force && php artisan amqp:listen & apache2-foreground"]
