<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AppServiceProvider extends ServiceProvider
{
    private $routing_keys = [
        'payment.validated',
    ];

    public function register()
    {
        // Register message broker
        $this->app->singleton(AMQPStreamConnection::class, function () {
            return new AMQPStreamConnection(env('MESSAGE_BROKER_HOST'), env('MESSAGE_BROKER_PORT'), env('MESSAGE_BROKER_USERNAME'), env('MESSAGE_BROKER_PASSWORD'));
        });
        $this->app->singleton(AMQPChannel::class, function () {
            return ($this->app->make(AMQPStreamConnection::class))->channel();
        });
    }

    public function boot(AMQPChannel $channel)
    {
        // Bootstrap default exchange and default queue
        $channel->exchange_declare(env('MESSAGE_BROKER_EXCHANGE'), env('MESSAGE_BROKER_EXCHANGE_TYPE'), false, true, false);
        $channel->queue_declare(env('MESSAGE_BROKER_DEFAULT_QUEUE'), false, true, false, true);
        // Bind all routing_keys
        foreach ($this->routing_keys as $routing_key) {
            $channel->queue_bind(env('MESSAGE_BROKER_DEFAULT_QUEUE'), env('MESSAGE_BROKER_EXCHANGE'), $routing_key);
        }
    }
}
