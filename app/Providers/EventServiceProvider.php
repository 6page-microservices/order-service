<?php

namespace App\Providers;

use App\Listeners\PaymentValidated;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'payment.validated' => [
            PaymentValidated::class,
        ],
    ];
}
