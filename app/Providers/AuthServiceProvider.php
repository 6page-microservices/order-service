<?php

namespace App\Providers;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Auth\GenericUser;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return GenericUser|null
     */
    public function boot(): ?GenericUser
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('eg-consumer-id')) {
                try {
                    // Retrieve UserInformation (Cached for 5 minutes)
                    return Cache::remember('authenticatedUser_' . $request->header('eg-consumer-id'), 300, function () use ($request) {
                        $client = new Client(['timeout' => 2, 'headers' => ['Authorization' => $request->header('Authorization')]]);
                        $response = $client->request('GET', env('USER_SERVICE') . '/api/users/' . $request->header('eg-consumer-id'));
                        $data = Validator::make(json_decode($response->getBody()->getContents(), true), [
                            'id' => 'required|uuid',
                            'firstname' => 'required|string|max:255',
                            'lastname' => 'required|string|max:255',
                            'username' => 'required|string|max:255',
                            'email' => 'nullable|email'
                        ])->validate();

                        return new GenericUser([
                            'id' => $data['id'],
                            'firstname' => $data['firstname'],
                            'lastname' => $data['lastname'],
                            'username' => $data['username'],
                            'email' => $data['email'],
                        ]);
                    });
                } catch (GuzzleException|Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        });

        return null;
    }
}
