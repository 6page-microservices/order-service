<?php

namespace App\SharedModels;

use App\Order;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Product extends Model implements SharedModel
{
    protected $table = 'products';
    protected $primaryKey = 'product_id';
    public $incrementing = false;
    protected $fillable = ['product_id', 'name', 'price'];

    /**
     * Synchronize the product model of the product-service (synchronously)
     *
     * @param int $productId
     * @return SharedModel
     * @throws GuzzleException
     */
    public static function synchronize($productId = 0): SharedModel
    {
        $httpClient = new Client(['timeout' => 2]);
        $response = $httpClient->request('GET', env('PRODUCT_SERVICE') . '/api/products/' . $productId);
        $data = Validator::make(json_decode($response->getBody()->getContents(), true), [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'id' => 'required|integer'
        ])->validate();

        $product = self::firstOrNew(['product_id' => $data['id']]);
        $product->price = $data['price'];
        $product->name = $data['name'];
        $product->save();

        return $product;
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'product_id', 'product_id');
    }
}
