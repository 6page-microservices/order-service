<?php

namespace App\SharedModels;

interface SharedModel
{
    /**
     * Synchronize remote model with local copy
     *
     * @param mixed|int $identifier identifier (typically primary key)
     * @return SharedModel
     */
    public static function synchronize($identifier = 0): SharedModel;
}
