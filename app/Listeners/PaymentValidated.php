<?php

namespace App\Listeners;

use App\Order;
use Exception;

class PaymentValidated
{
    /**
     * Handle PaymentValidated Event
     * @param $messageBody
     * @return mixed
     * @throws Exception
     */
    public function handle($messageBody)
    {
        $order = json_decode($messageBody);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception(json_last_error_msg());
        }

        $order = Order::findOrFail($order->id);
        $order->paid = true;
        $order->save();

        return $order;
    }
}
