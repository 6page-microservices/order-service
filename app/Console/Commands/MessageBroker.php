<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Channel\AMQPChannel;

class MessageBroker extends Command
{
    protected $signature = 'amqp:listen';
    protected $description = 'Listen for messages';
    private $retries = 1;
    private $maxRetries = 5;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(AMQPChannel $channel)
    {
        echo "[Info]\tWaiting for messages ...\n";

        $channel->basic_consume('order-service', '', false, false, false, false, function ($message) use ($channel) {
            echo("[" . $message->delivery_info['routing_key'] . "]\t" . $message->body . "\n");
            try {
                Event::dispatch($message->delivery_info['routing_key'], $message->body);
                $channel->basic_ack($message->delivery_info['delivery_tag']); // Not executed if event throws an exception
            } catch (Exception $e) {
                if ($this->retries <= $this->maxRetries) {
                    $channel->basic_reject($message->delivery_info['delivery_tag'], true); // Reject and requeue
                    echo "[Notice]\tRetry " . $this->retries . "/" . $this->maxRetries . "\n";
                    echo "[Error]\t" . $e->getMessage() . "\n";
                }
                Log::error($e);
                if ($this->retries > $this->maxRetries) {
                    $channel->basic_reject($message->delivery_info['delivery_tag'], false); // Reject but do not requeue
                    echo "[Notice]\tMessage rejected\n";
                    $this->retries = 1;
                } else {
                    $this->retries++;
                }
            }
        });

        while ($channel->is_consuming()) {
            $channel->wait();
        }

        $channel->close();
        $channel->getConnection()->close();
    }
}
