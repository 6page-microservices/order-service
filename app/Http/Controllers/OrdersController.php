<?php

namespace App\Http\Controllers;

use App\Order;
use App\SharedModels\Product;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class OrdersController extends Controller
{
    public function findAll(Request $request): JsonResponse
    {
        return response()->json(
            Order::with(['product'])
                ->where('user_id', $request->user()->id)
                ->get(),
            200
        );
    }

    public function create(Request $request, AMQPChannel $channel): JsonResponse
    {
        $this->validate($request, [
            'product_id' => 'required|integer'
        ]);

        try {
            // Synchronize product data before order creation to make sure that no stale product price is used
            // This synchronous remote operation is intended: Prefer consistency over availability (CP system)
            $product = Product::synchronize($request->product_id);
            $order = new Order();
            // Total sum is saved separately because the product price may change
            $order->total_sum = $product->price;
            $order->product_id = $product->product_id;
            $order->user_id = $request->user()->id;
            $order->save();

            $channel->basic_publish(new AMQPMessage($order->toJson()), env('MESSAGE_BROKER_EXCHANGE'), 'order.created');

            return $this->findOne($request, $order->id);
        } catch (Exception|GuzzleException $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => [
                'code' => 503,
                'message' => 'Orders are currently unavailable, please try again later: ' . $e->getMessage()
            ]], 503);
        }
    }

    public function findOne(Request $request, int $orderId): JsonResponse
    {
        return response()->json(
            Order::with(['product'])
                ->where('user_id', $request->user()->id)
                ->findOrFail($orderId),
            200
        );
    }
}
